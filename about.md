---
layout: page
title: About
permalink: /about/
---

I'm an Computer Science and Engineering student at UCLA, class of 2016. I enjoy developing and working with a wide array of languages and devices, from programming challenges in '(scheme) to integrating DLP for cutting-edge structured illumination microscopy research. 

### Contact me

[acmiyaguchi@gmail.com](mailto:acmiyaguchi+blog@gmail.com)