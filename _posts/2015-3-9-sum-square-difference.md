---
layout: post
title: Sum square difference
---

I've been messing around with some math riddles recently, which inspired me to come back to Project Euler questions. After getting through the first questions, I stumbled on [problem 6](https://projecteuler.net/problem=6), which describes the sum square difference problem. The problem can be summed up succinctly as follows:

$$ \sum_{k=0}^{n} k^2 - (\sum_{k=0}^{n} k)^2 $$

where \\(n=100\\). This problem is incredibly simple to brute force, and for most cases it can be solved for very large inputs on a modern system. 

### The Naive Solution
We can go about solving the given problem in a very straight-forward manner by implementing the given expression in code. Below is a code snippet of the brute force method of solving the problem written in [Racket](http://racket-lang.org/).

{% highlight racket %}
(define (square n)
  (* n n))

(define (square-of-sums n)
  (square 
   (for/sum ([x (in-range (+ n 1))]) x)))

(define (sum-of-squares n)
  (for/sum ([x (in-range (+ n 1))]) (square x)))

;; The naive approach in linear time
(define (sum-square-diff n) 
  (- (square-of-sums n) (sum-of-squares n)))
{% endhighlight %}

Where's the fun in brute forcing something like this. In particular, there is a way to reduce this linear time solution into a constant time solution by deriving a closed form expression for the given expression. It took me a bit of time to come to a solution, so I've detailed what I've learned from this experience. 

### Deriving the closed form of the arithmetic summation
It's very likely that you have already encountered a very common closed form of a simple arithmetic series back in high school algebra II or trigonometry. A closed form expression for a summation simply means we can find the sum of any arbitrary range of a series using a finite number of operations. Unfortunately, the sum of squares is not a simple arithmetic series, but we can derive the solution in a similar fashion. 

In order to elucidate the methodology we use to find the closed form of the sum of squares, let us prove 

$$\sum_{k=0}^{n} k = \frac{n(n+1)}{2} $$

using a tabulated method that will be useful later on. For me, the most intuitive way to find the solution was to observe a pattern between successive terms in the series.


| \\(n\\)        | 0 | 1 | 2 | 3 | 4  | 5  |
|----------------|---|---|---|---|----|----|
|    \\(S_n\\)   | 0 | 1 | 3 | 6 | 10 | 15 |
| \\(\Delta_1\\) |   | 1 | 2 | 3 | 4  | 5  |
| \\(\Delta_2\\) |   |   | 1 | 1 | 1  |    |


By observation, we can see that our function will be in the form \\(an^2+bn+c\\) since our second order difference \\(\Delta_2\\) is constant with an increasing n. 

$$ f(0) = 0^2a + 0b + c = 0 $$
$$ f(1) = 1^2a+ 1b + c = 1 $$
$$ f(2) = 2^2a + 2b + c =3$$

By solving the system of equations for the first few iterations, we come to the conclusion that \\(a = b = \frac{1}{2}; c = 0\\). The closed form of the summation is therefore 

$$ S_n = \frac{n^2}{2} + \frac{n}{2} = \frac{n(n+1)}{2}$$

### Closed form of the sum of squares
We can apply the same methodology as above to derive a closed form of the sum of squares. Below is the tabulated differences between the sequences.

| \\(n\\)        | 0 | 1 | 2 | 3  | 4  | 5  |
|----------------|---|---|---|----|----|----|
|    \\(n^2\\)   | 0 | 1 | 4 | 9  | 16 | 25 |
| \\(S_n\\)      | 0 | 1 | 5 | 14 | 30 | 55 |
| \\(\Delta_1\\) |   | 1 | 4 | 9  | 16 | 25 |
| \\(\Delta_2\\) |   |   | 3 | 5  | 7  | 9  |
| \\(\Delta_3\\) |   |   |   | 2  | 2  | 2  |

We note that this series will be a 3rd order function of form \\(an^3+bn^2+cn+d\\) since successive differences at \\(\Delta_3\\) are constant with increasing n. Solving for the system of equations we get that \\(a=\frac{1}{3}; b=\frac{1}{2}; c =\frac{1}{6}; d=0\\). The closed form solution for the sum of squares is thusly

$$ S_n = \frac{n^3}{3}+\frac{n^2}{2}+\frac{n}{6} = \frac{n(n+1)(2n+1)}{6} $$ 

We can prove that this formula is correct by induction. We prove that this formula is correct for the \\(n=1\\) case:

$$ S_1 = \frac{1(2)(3)}{6} = 1$$

Then, assuming \\(S_n\\) is true, we prove that \\(S_{n+1}\\) is also true:

$$ 
    S_{n+1} = \frac{(n+1)(n+2)(2n+3)}{6} = 
    \frac{n^3}{3}+\frac{3n^2}{2}+\frac{13n}{6} + 1
$$

$$
    \sum_{k=0}^{n+1} k^2 = (n+1)^2 + \sum_{k=0}^{n} k^2 = \frac{n^3}{3}+\frac{3n^2}{2}+\frac{13n}{6} + 1 
$$

Since the \\(n+1\\) case is also true, this closed form equation is correct for this sequence by induction.

### The closed form implementation
Now we know everything we need to solve this problem in constant time. Using the above, our expression for the difference of the sum of squares and the square of sums is as follows:

$$ \frac{(n+1)(n+2)(2n+3)}{6} - \frac{n^2(n+1)^2}{4} $$

We can now write a short program that implements the closed form solution we have just derived.

{% highlight racket %}
;; The derived closed form solution
;; n(n+1)(2n+1)/6 - n^2(n+1)^2/4
(define (sum-square-diff-closed n)
  (- (/ (+ 
         (expt n 4) 
         (* 2 (expt n 3)) 
         (expt n 2))
        4)
     (/ (+ 
         (* 2 (expt n 3)) 
         (* 3 (expt n 2)) n) 
        6)))
{% endhighlight %}

We can do some quick profiling on the code to see the difference between the two implementations. Racket provides a useful profiling tool that suits our needs well. 

{% highlight racket %}
(require profile)
(define n 10000000)
(profile-thunk (thunk (sum-square-diff n)))
(profile-thunk (thunk (sum-square-diff-closed n)))
{% endhighlight %}


{% highlight racket %}
Welcome to DrRacket, version 6.1 [3m].
Language: racket; memory limit: 128 MB.
Profiling results
-----------------
  Total cpu time observed: 2641ms (out of 2735ms)
  Number of samples taken: 48 (once every 55ms)
  2500000166666641666665000000
  
Profiling results
-----------------
  Total cpu time observed: 0ms (out of 0ms)
  Number of samples taken: 0 (once every 0ms)
  2500000166666641666665000000
{% endhighlight %}

We observe the difference between the two only when n is significantly large. Even with \\(n=10,000,000\\), there is only a 2.6 second difference. 

Despite its uselessness, deriving a constant time solution to this Project Euler problem has been an entertaining exercise. Code from this page can be found on my github repository below.

#### References
* [Solution on my github](https://github.com/acmiyaguchi/programming-challenges/blob/master/euler/06-sum_square_difference.rkt)
* [Problem 6 from the Project Euler Archives](https://projecteuler.net/problem=6)
* [Several methodologies of solving the sum of squares](http://www.trans4mind.com/personal_development/mathematics/series/sumNaturalSquares.htm)
