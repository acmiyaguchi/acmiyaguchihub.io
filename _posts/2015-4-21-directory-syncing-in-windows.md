---
layout: post
title: A rant on directory syncing in windows
---

I've always enjoyed using git as my version control system of choice. It's simple in concept and the branching mechanism that its known for has made it a pleasure to use in the various small projects that I've done. But at work, I haven't necessarily had a choice in that department. 

As a beginner of the tool, I wasn't the biggest fan. I learned the basics of mapping my work directories, checking out files to make changes, making changelists, and submitting them back into the central repository. It seems like you never really appreciate all these extra features until you have a use for them.

Since then, I've slowly began to appreciate the differences the benefits of perforce. While developing on my machine, I had a small annoyance of syncing my perforce workspace files with the files deployed on my virtual machine for testing. Every change to a script had to be followed by a tedious steps of copying and pasting files over. I couldn't simply create a symbolic link to the folder because there were 4GB of dependencies that would have become clutter in my working directory.

I initially toyed with the idea of a batch script to copy over files. In the same vein, I also thought of making a small script to poll for file changes in my development folder and sync them over to the VM folder. However, this was a one way operation, I could sync over file changes, but deletions wouldn't have been handled very easily. I wanted a direct mapping of one folder's contents into another folder, mirroring changes, additions, and deletions. 

With enough google-fu, I eventually came upon [Microsoft SyncToy](http://en.wikipedia.org/wiki/SyncToy), which is a utility that synchronizes folders and files using the Microsoft Sync Framework. After making a directory mapping, all it took was one click to sync up the folders. The annoying itch that I had was no longer with this tool. 

One day, if I find the time and need for it, I would like to develop a tool that simulates the workspace mapping functionality of perforce as a cross platform system daemon or background process. I still have to click on the run button after all. If the native .NET framework wasn't such a pain to script in, I would try to make a hook into one of my editors to sync on save. If only I ever find that time.